#!/bin/bash

#force Wlan0 set-up to grab static IP
sudo ifdown --force wlan0
sudo ifup --force wlan0

#git fetch and pull from repo
sudo sh -c 'cd /home/pi/apollo360_server/ && sudo git fetch && sudo git pull'

#start MJPEG stream (ensure script is executable!)
#sudo bash /home/pi/apollo360_server/Backend/RPiMJPEG.sh &
#hopefully now integrated into server.js :)

#start server and pipe output 
sudo node /home/pi/apollo360_server/BackEnd/server.js > serverLOG.txt

exit 0
