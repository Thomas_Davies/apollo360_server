/*
Hub of all communications!

Communicates with front end users via express routing and socket.io for realtime messaging of controls/sensor data
Communicates with ballbot via serial link
	-requests sensor data
	-sends desired motor speeds (arduino handles maintaining heading and acceleration curves)

written by: Thomas Davies
 */

var PORT = 3030;

var express = require('express');
var ip = require("ip");
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var path = require('path');
var isSerialConnected = false;
var httpReq = require('http');

var rxBuffer="";
var isCommandRecievedSincePreviousPost = false;	//used to track status of bot. 

var currentController = {
	socket 		: 0,
	nickname	: 0,
	ip			: 0
};

//this should be refactored out you noob
var controlPassword = "ballbot";

//express gives access to all dem files! otherwise stupid errors everywhere :)
var basePath = path.normalize(path.join(__dirname,'..'));
var landingPage = path.join(basePath + '/FrontEnd/landing_page.html');
var viewerPage = path.join(basePath + '/FrontEnd/viewer_page.html');
var controllerPage = path.join(basePath + '/FrontEnd/controller_page.html');
var aboutPage = path.join(basePath + '/FrontEnd/about_page.html');
var piCameraStreamURL = path.join(basePath + '/FrontEnd/images/image_stream.jpg');
var staticPath = path.join(basePath + '/FrontEnd');

app.use(express.static(staticPath));

var spawn = require('child_process').spawn;
var proc;
var postPeriod = 60;

//~~~~~~~~~~~~~~~~~~~~~~~~~SeeControl Post Request~~~~~~~~~~~~~~~


function httpPost2SeeControl(msg) {
	var request = require('request');
	request(
		{
			url: 'http://com2.sl.seecontrol.com:16360/json',
			method: 'POST',
			'content-type': 'application/json',
			body: JSON.stringify(msg)
		},
		function POSTcallback(error,response,body){
			if(error){
				console.log("awe shucks...it failed: "+error);
			} else {
				console.log(response.statusCode,body);
			}
		}
	);
};


//default route that will display index.html
app.get('/',function landingRequestHander(req,res) {
	res.sendFile(landingPage);
});

app.get('/controller',function controllerRequestHandler(req,res){
	var userIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("attempted connection to controller: " + userIP);
	if (userIP == currentController.ip){
		res.sendFile(controllerPage);
	}
	else {
		res.redirect('/');	//send back to landing page.
	}
});

app.get('/viewer',function viewerRequestHandler(req,res){
	res.sendFile(viewerPage);
});


app.get('/about',function aboutRequestHandler(req,res){
	res.sendFile(aboutPage);
});


//where the server listens for connections
http.listen(PORT,function() {
	console.log('listening on *:');
	console.log(PORT);
});



//*************SOCKET.IO IMPLEMENTATION*****************//
var sockets = {};

//new connection event, create new socket and save to global
io.on('connection',function(socket){
	sockets[socket.id] = socket;
	console.log("Total Clients Connected: ", Object.keys(sockets).length);

	socket.on('disconnect', function() {
		delete sockets[socket.id];

		if (socket.id == currentController.socket) {
			//reset controller
			currentController = {
				socket 		: 0,
				nickname	: 0,
				ip			: 0
			};
		}
	});

	socket.on('direction_change', function(msg) {
		isCommandRecievedSincePreviousPost = true;	

		var rxJsonMsg = JSON.parse(msg);	//parse the incoming json message emitted!
		io.sockets.emit('direction_change',JSON.stringify(rxJsonMsg));	//re emit for any viewer pages!
		//throw updated control values in outgoing message to arduino controller
		uartTxJsonMsg = JSON.parse('{"direction":0,"speed":0}');	//just set motor speed. Otherwise we're waiting for large json dump!
		uartTxJsonMsg["direction"] = rxJsonMsg["direction"];
		if (uartTxJsonMsg["direction"] == -1) {
			uartTxJsonMsg["direction"] = 0;
		}
		uartTxJsonMsg["speed"] = rxJsonMsg["speed"];


		console.log(uartTxJsonMsg);
		if ((rxJsonMsg != null) && (isSerialConnected)){
			serialPort.write(JSON.stringify(uartTxJsonMsg));
		}
	});

	socket.on('password_entered', function(credentials){
		var rxJsonCredentials = JSON.parse(credentials);
		console.log(rxJsonCredentials["name"] + ' entered password: ' + rxJsonCredentials["password"]);

		if (rxJsonCredentials["password"] == controlPassword) {
			//check if someone else in control
			if (currentController.socket == 0) {
				currentController = {
					socket 		: socket.id,
					nickname	: rxJsonCredentials["name"],
					ip			: socket.request.connection.remoteAddress
				};
				socket.emit('access_granted','granted');
			}
			else
			{
				socket.emit('access_granted',currentController.nickname);	//send back current controllers nickname
			}			
		}
		else {
			socket.emit('access_granted','denied');
		}

	});

});

//*******************RPi<-->Arduino**************************//

var seeControlJsonPost = JSON.parse(
	'{"target":"Apollo360","code":0,"values":{"status":"active","kPa":1,"C":1,"V":1,"RGB":[1,1,1],"RH":1}}'
);

var seeControlCount = 0;

var SerialPort = require("serialport").SerialPort;
var serialPort = new SerialPort("/dev/ttyACM0", {
	baudrate: 115200
},false);	//open immediatly flag

serialPort.on('error', function(err){
	console.log(err);
});

serialPort.open(function(err) {
	if (err) {
		console.log(err);
		return;
	}

	console.log("Arduino Serial Connection Established")

	//toggle to allow post requests serial access
	isSerialConnected = true;

	//REQUEST FOR DATA (once per second)
	setInterval(function() {
		//sensors requested by unit desired. Temperature = Deg Celcius, Humidity = %RH etc...
		uartTxJsonMsg = JSON.parse('{"sensors":1,"kPa":1,"C":1,"V":1,"RGB":1,"%RH":1}');
		serialPort.write(JSON.stringify(uartTxJsonMsg));
		seeControlCount++;
	},1000);


	//Handle response with data!
	serialPort.on('data',function(data) {
		rxBuffer += data.toString();
		
		if (rxBuffer.indexOf("{")>-1 && rxBuffer.indexOf("}")>rxBuffer.indexOf("{") ) {
			var split = rxBuffer.split("}");
			rxBuffer = split[0] + "}";
			var uartRxJsonMsg = JSON.parse(rxBuffer);
			//Send sensor data to front end.
			io.sockets.emit('sensor_update',JSON.stringify(uartRxJsonMsg));
			//format data for posting to seecontrol

			//transition from standby post frequency to active post frequency. IMMEDIATLY :)
			if (seeControlJsonPost["values"]["status"] == "standby" && isCommandRecievedSincePreviousPost) {
				seeControlCount = postPeriod +1;
			}
		
			//post to seeControl every postPeriod seconds.
			if (seeControlCount > postPeriod) {
				seeControlJsonPost["values"]["kPa"] = uartRxJsonMsg["kPa"]/10;
				seeControlJsonPost["values"]["C"] = uartRxJsonMsg["C"];
				seeControlJsonPost["values"]["V"] = (uartRxJsonMsg["V"]/1023)*12;
				for (var i=0; i<3; i++) seeControlJsonPost["values"]["RGB"][i] = uartRxJsonMsg["RGB"][i];
				seeControlJsonPost["values"]["RH"] = uartRxJsonMsg["%RH"];

				//active if control command recieved since last post OR speed is greater then 0 (for case where user holds key)
				if (isCommandRecievedSincePreviousPost || (uartRxJsonMsg["speed"] > 0)) {
					seeControlJsonPost["values"]["status"] = "active";
					postPeriod = 5;
				} else {
					seeControlJsonPost["values"]["status"] = "standby";
					postPeriod = 60;
				}
				
				isCommandRecievedSincePreviousPost = false;

				console.log("posting to seeControl body: " + JSON.stringify(seeControlJsonPost));
				//SEND POST REQUEST
				httpPost2SeeControl(seeControlJsonPost);

				seeControlCount = 0;
			}

			rxBuffer = "";	//reset buffer
		}
		else if (rxBuffer.indexOf("}") > rxBuffer.indexOf("{")) {
			//...ostrich algorithm...
			rxBuffer = "";

		} 


	});
});

//****************************Startup MJPEG Camera Stream*********************//
var sys = require('sys');
var exec = require('child_process').exec;

function puts(error,stdout,stderr) {sys.puts(stdout)}
exec("/home/pi/apollo360_server/BackEnd/RPiMJPEG.sh",puts);
