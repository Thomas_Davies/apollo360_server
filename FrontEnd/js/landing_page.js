/**
 * Created by t_davit on 8/14/2015.
 */

var socket = io();
console.log('CONNECTED');

var controllerImg = document.getElementById('img_controller');
var viewerImg = document.getElementById('img_viewer');
var aboutImg = document.getElementById('img_about');

//Event listener for user clock on Controller button
controllerImg.addEventListener("click", function handleControllerClick() {
    //controller view requested.
    var nickname = prompt("What should I call you?");
    var password = prompt("Please enter control password");

    //send password back to server
    var credentials = JSON.parse('{"password" : 1 ,"name": "t"}');
    credentials["password"] = password;
    credentials["name"] = nickname;
    console.log("nickname: " + nickname + " pass: " + password);
    socket.emit('password_entered',JSON.stringify(credentials));

    socket.on('access_granted',function socketCredentialCallback(verified){
        if (verified == 'granted'){
            //serve up controller page.
            //verification check also done on backend.
            document.location.href = window.location.href + "controller";
        }
        else if (verified == 'denied'){
            alert("Password is incorrect. You don't have access foo.");
        }
        else
        {
           alert("Sorry, " + verified + " is currently controlling the bot.");
        }
    });
});

//Event listener for user click on Viewer button
viewerImg.addEventListener("click" ,function handleViewerClick()  {
    //just redirect the user to viewer page!
    //no verification needed as this is an open access page (for now)
    document.location.href = window.location.href + "viewer";
});

//Event listener for user click on About button
aboutImg.addEventListener("click",function handleAboutClick() {
    //TODO: create and about page with embedded link to youtube video of project.
    document.location.href = window.location.href + "about";
});

