var socket = io();

var keyFlagsWASD = [0,0,0,0];
var botSpeed = 100;			//actual motor speed to be sent to bot...ranges from 1 to 100
var botDirection = 0;				//in radians (where 0 is the right wheel when facing forward...if that makes sense)
var PI = 3.14;				//constant DO NOT CHANGE ME
var prevKeySum = -1;			//previous key pressed to ensure only one message sent per change
var txJsonMsg = JSON.parse('{"direction":0,"speed":0}')



//HTML Item References
//array of game pad arrows
var gamepadArrows = {
	NArrowPressed	:	document.getElementById('img_Narrow_pressed'),
	NArrowNeutral	:	document.getElementById('img_Narrow_neutral'),
	NEArrowPressed	:	document.getElementById('img_NEarrow_pressed'),
	NEArrowNeutral	:	document.getElementById('img_NEarrow_neutral'),
	EArrowPressed	:	document.getElementById('img_Earrow_pressed'),
	EArrowNeutral	:	document.getElementById('img_Earrow_neutral'),
	SEArrowPressed	:	document.getElementById('img_SEarrow_pressed'),
	SEArrowNeutral	:	document.getElementById('img_SEarrow_neutral'),
	SArrowPressed	:	document.getElementById('img_Sarrow_pressed'),
	SArrowNeutral	:	document.getElementById('img_Sarrow_neutral'),
	SWArrowPressed	:	document.getElementById('img_SWarrow_pressed'),
	SWArrowNeutral	:	document.getElementById('img_SWarrow_neutral'),
	WArrowPressed	:	document.getElementById('img_Warrow_pressed'),
	WArrowNeutral	:	document.getElementById('img_Warrow_neutral'),
	NWArrowPressed	:	document.getElementById('img_NWarrow_pressed'),
	NWArrowNeutral	:	document.getElementById('img_NWarrow_neutral')
};


var txt_Pressure = document.getElementById('txt_pressure');
var txt_Battery = document.getElementById('txt_battery');
var txt_Temperature = document.getElementById('txt_temperature');
var txt_Light = document.getElementById('txt_light');
var txt_Humidity = document.getElementById('txt_humidity');

var speedSlider = document.getElementById('slider_speed');

var piCameraImg = document.getElementById('img_piCamera');

var img_Viewer = document.getElementById('img_viewer');
var img_About = document.getElementById('img_about');

//set to all unpressed.
gamepadUpdater(0);

img_About.addEventListener("click", function aboutImgClickHandler(){
	//get domain with no depth
	var domain = window.location.href.split("/");

	document.location.href = domain[0] + "/about";
});
img_Viewer.addEventListener("click", function viewerImageClickHandler(){
	var domain = window.location.href.split("/");
	document.location.href = domain[0] + "/viewer";
});


self.addEventListener("keydown",keyDownHandler,false);
self.addEventListener("keyup",keyUpHandler,false);
speedSlider.addEventListener("change", function speedSliderChangeHandler() {
	botSpeed = speedSlider.value();
},false);



function keyDownHandler(e){
	//triggered when keyboard key pressed down

	//why the weird numbers? Its so the switch case can be used to easily differentiate between combos! BIT BANGING WOO
	switch(e.keyCode)
	{
		case 87:
			//W key pressed
			keyFlagsWASD[0] = 1;
			break;
		case 65:
			//A key pressed
			keyFlagsWASD[1] = 2;
			break;
		case 83:
			//S key pressed
			keyFlagsWASD[2] = 4;
			break;
		case 68:
			//D key pressed
			keyFlagsWASD[3] = 8;
			break;
		default:
			break;
	}

	updateMotorSpeeds();

}

function keyUpHandler(e) {
	switch(e.keyCode)
	{
		case 87:
			//W key pressed
			keyFlagsWASD[0] = 0;
			break;
		case 65:
			//A key pressed
			keyFlagsWASD[1] = 0;
			break;
		case 83:
			//S key pressed
			keyFlagsWASD[2] = 0;
			break;
		case 68:
			//D key pressed
			keyFlagsWASD[3] = 0;
			break;
		default:
			break;
	}

	updateMotorSpeeds();
}

function updateMotorSpeeds() {
	//use the key flag array to determine what keys are pressed and set angle/magnitude
	//just go through each case and determine angles yo
	var keySum = keyFlagsWASD[0] + keyFlagsWASD[1] + keyFlagsWASD[2] + keyFlagsWASD[3];

	//only emits if same message hasn't already emmitted. (may want to change in case of failed emits? need to test)
	if (keySum != prevKeySum) {
		prevKeySum = keySum;

		botSpeed = $('#slider_speed').slider("option", "value");
		switch (keySum) {
			case 1:
				//W key pressed
				botDirection = PI/4;
				break;
			case 2:
				//A key pressed
				botDirection = 3*PI/4;
				break;
			case 3:
				//W & A pressed
				botDirection = PI/2;
				break;
			case 4:
				//S pressed
				botDirection = 5*PI/4;
				break;
			case 6:
				//S and A pressed
				botDirection =4*PI/4;
				break;
			case 8:
				//D pressed
				botDirection = 7*PI/4;
				break;
			case 9:
				//D and W pressed
				botDirection = 0;
				break;
			case 12:
				//S & D pressed
				botDirection = 3*PI/2;
				break;
			default:
				//stop the motors
				botSpeed = 0;
				botDirection = -1;
				break;
		}
		txJsonMsg['direction'] = botDirection;
		txJsonMsg['speed'] = botSpeed;
		gamepadUpdater(keySum);
		socket.emit('direction_change',JSON.stringify(txJsonMsg));
	}
}


socket.on('sensor_update',function(msg){
	var rxJsonMsg = JSON.parse(msg);
	//update UI with new readings
	txt_Temperature.innerHTML = rxJsonMsg["C"] + " C";
	//TODO: some visualisation of RGB data, just a circle that updates with new RGB data to color it?
	txt_Pressure.innerHTML = (rxJsonMsg["kPa"]/10).toFixed(3) + " kPa"; //unit kPa
	txt_Battery.innerHTML = rxJsonMsg["V"] + " V";			//unit Voltage (V)
	txt_light.innerHTML = "R: " + rxJsonMsg["RGB"][0] + "<br/>" + "G: " + rxJsonMsg["RGB"][1] + "<br/>" + "B: " +rxJsonMsg["RGB"][2]
	txt_Humidity.innerHTML = rxJsonMsg["%RH"] + " %RH";		//unit %RH
});


//really unelegant code block to hide/show pressed arrow keys.
function gamepadUpdater(keySum) {
		switch (keySum) {
			case 1:
				//W key pressed (N)
				gamepadArrows.NArrowPressed.style.visibility 	= 'visible';
				gamepadArrows.NEArrowPressed.style.visibility	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
				break;

			case 2:
				//A key pressed (W)
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'visible';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';

				break;
			case 3:
				//W & A pressed (NW)
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'visible';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'hidden';

				break;
			case 4:
				//S pressed (S)
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'visible';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';

				break;
			case 6:
				//S and A pressed (SW)
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'visible';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'hidden';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';

				break;
			case 8:
				//D pressed (E)
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'visible';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';

				break;
			case 9:
				//D and W pressed (NE)
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'visible';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'hidden';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';

				break;
			case 12:
				//S & D pressed (SE)
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'visible';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'hidden';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';

				break;
			default:
				//stop the motors
				gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
				gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

				gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
				gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';

				break;
		}
}

//refresh img ever X ms
setInterval(function() {
	//update camera view
	piCameraImg.src = 'images/picamera.jpg?rand=' + Math.random();
},100);






