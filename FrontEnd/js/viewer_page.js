var socket = io();

var PI = 3.14; //const

var txt_Speed = document.getElementById('txt_speed');
var txt_Pressure = document.getElementById('txt_pressure');
var txt_Battery = document.getElementById('txt_battery');
var txt_Temperature = document.getElementById('txt_temperature');
var txt_Light = document.getElementById('txt_light');
var txt_Humidity = document.getElementById('txt_humidity');

var piCameraImg = document.getElementById('img_piCamera');

var img_Controller = document.getElementById('img_controller');
var img_About = document.getElementById('img_about');

//HTML Item References
//array of game pad arrows
var gamepadArrows = {
	NArrowPressed	:	document.getElementById('img_Narrow_pressed'),
	NArrowNeutral	:	document.getElementById('img_Narrow_neutral'),
	NEArrowPressed	:	document.getElementById('img_NEarrow_pressed'),
	NEArrowNeutral	:	document.getElementById('img_NEarrow_neutral'),
	EArrowPressed	:	document.getElementById('img_Earrow_pressed'),
	EArrowNeutral	:	document.getElementById('img_Earrow_neutral'),
	SEArrowPressed	:	document.getElementById('img_SEarrow_pressed'),
	SEArrowNeutral	:	document.getElementById('img_SEarrow_neutral'),
	SArrowPressed	:	document.getElementById('img_Sarrow_pressed'),
	SArrowNeutral	:	document.getElementById('img_Sarrow_neutral'),
	SWArrowPressed	:	document.getElementById('img_SWarrow_pressed'),
	SWArrowNeutral	:	document.getElementById('img_SWarrow_neutral'),
	WArrowPressed	:	document.getElementById('img_Warrow_pressed'),
	WArrowNeutral	:	document.getElementById('img_Warrow_neutral'),
	NWArrowPressed	:	document.getElementById('img_NWarrow_pressed'),
	NWArrowNeutral	:	document.getElementById('img_NWarrow_neutral')
};

//sset to defauklt state 
gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';


//~~~~~~~~~~~~~Event Listeners~~~~~~~~~~~~~~~~~~~~//
img_Controller.addEventListener("click", function controllerImgClickHandler(){
	//controller view requested.
    var nickname = prompt("Enter Name");
    var password = prompt("Enter Password");

    //send password back to server
    var credentials = JSON.parse('{"password" : 1 ,"name": "t"}');
    credentials["password"] = password;
    credentials["name"] = nickname;
    socket.emit('password_entered',JSON.stringify(credentials));

    socket.on('access_granted',function socketCredentialCallback(verified){
        if (verified == 'granted'){
            //serve up controller page.
            //verification check also done on backend.
            document.location.href = window.location.href + "../controller";
        }
        else if (verified == 'denied'){
            alert("Password is incorrect. You don't have access foo.");
        }
        else
        {
           alert("Sorry, " + verified + " is currently controlling the bot.");
        }
    });
});

img_About.addEventListener("click", function aboutImgClickHandler(){
	var domain = window.location.href.split("/");
	document.location.href = domain[0] + "../about";
});


//~~~~~~~~~~~~~SOCKET.IO LISTENERS~~~~~~~~~~~~~~~~~~~~~~//
socket.on('sensor_update',function(msg){
	//console.log(msg);
	//Parse Json message from server
	var rxJsonMsg = JSON.parse(msg);
	//update UI with new readings
	txt_Temperature.innerHTML = rxJsonMsg["C"] + " C";
	//TODO: some visualisation of RGB data, just a circle that updates with new RGB data to color it?
	txt_Pressure.innerHTML = rxJsonMsg["kPa"] + " kPa"; //unit kPa
	txt_Battery.innerHTML = rxJsonMsg["V"] + " V";			//unit Voltage (V)
	txt_light.innerHTML = "R: " + rxJsonMsg["RGB"][0] + "<br/>" + "G: " + rxJsonMsg["RGB"][1] + "<br/>" + "B: " +rxJsonMsg["RGB"][2]
	txt_Humidity.innerHTML = rxJsonMsg["%RH"] + " %RH";		//unit %RH
});

socket.on('direction_change',gamepadUpdater);

//really unelegant code block to hide/show pressed arrow keys.
function gamepadUpdater(msg) {
		//console.log(msg);
		var angle = JSON.parse(msg);
		console.log(angle);
		//find which angle sent (approximatly equal to)
		if (Math.abs(angle - PI/2) < 0.1){
			//W key pressed (N)
			gamepadArrows.NArrowPressed.style.visibility 	= 'visible';
			gamepadArrows.NEArrowPressed.style.visibility	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
		}
		else if(Math.abs(angle - PI) < 0.1) {
			//A key pressed
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'visible';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
	}
		else if(Math.abs(angle - 3*PI/4) < 0.1) {
			//W & A pressed
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'visible';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'hidden';

		}
		else if(Math.abs(angle - 3*PI/2) < 0.1) {
			//S pressed (S)
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'visible';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
	
		}
		else if(Math.abs(angle - 5*PI/4) < 0.1) {

			//S and A pressed (SW)
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'visible';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'hidden';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
		}
		else if(Math.abs(angle) < 0.1) {
			//D pressed (E)
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'visible';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
		}
		else if(Math.abs(angle - PI/4) < 0.1) {
			//D and W pressed (NE)
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'visible';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'hidden';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
		}
		else if(Math.abs(angle - 7*PI/4) < 0.1) {
			//S & D pressed (SE)
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'visible';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'hidden';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
		}
		else if(angle == -1) {
			gamepadArrows.NArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.EArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SEArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.SWArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.WArrowPressed.style.visibility 	= 'hidden';
			gamepadArrows.NWArrowPressed.style.visibility 	= 'hidden';

			gamepadArrows.NArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.EArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SEArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.SWArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.WArrowNeutral.style.visibility 	= 'visible';
			gamepadArrows.NWArrowNeutral.style.visibility 	= 'visible';
		}

}

//refresh img ever X ms
setInterval(function() {
	//update camera view
	piCameraImg.src = 'images/picamera.jpg?rand=' + Math.random();
},100);








