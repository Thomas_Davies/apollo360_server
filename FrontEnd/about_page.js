/**
 * Created by t_davit on 9/2/2015.
 */

var img_Controller = document.getElementById('img_controller');
var img_About = document.getElementById('img_about');

//~~~~~~~~~~~~~Event Listeners~~~~~~~~~~~~~~~~~~~~//
img_Controller.addEventListener("click", function controllerImgClickHandler(){
    //controller view requested.
    var nickname = prompt("Enter Name");
    var password = prompt("Enter Password");

    //send password back to server
    var credentials = JSON.parse('{"password" : 1 ,"name": "t"}');
    credentials["password"] = password;
    credentials["name"] = nickname;
    socket.emit('password_entered',JSON.stringify(credentials));

    socket.on('access_granted',function socketCredentialCallback(verified){
        if (verified == 'granted'){
            //serve up controller page.
            //verification check also done on backend.
            document.location.href = window.location.host + "controller";
        }
        else if (verified == 'denied'){
            alert("Password is incorrect. You don't have access foo.");
        }
        else
        {
            alert("Sorry, " + verified + " is currently controlling the bot.");
        }
    });
});

img_About.addEventListener("click", function aboutImgClickHandler(){
    document.location.href = window.location.host + "about";
});